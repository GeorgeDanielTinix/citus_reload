#!/bin/bash                                                                                                                                                                       # This file is meant to be executed via systemd.                                                                                                                                  
source /usr/local/rvm/scripts/rvm                                                                                                                                                 
source /etc/profile.d/rvm.sh                                                                                                                                                      
export ruby_ver=$(rvm list default string)                                                                                                                                        
                                                                                                                                                                                  
export CONFIGURED=yes                                                                                                                                                             
export TIMEOUT=50                                                                                                                                                                 
export APP_ROOT=/home/rails/citus_reload                                                                                                                                        
export RAILS_ENV="production"                                                                                                                                                     
export GEM_HOME="/home/rails/citus_reload/vendor/bundle"                                                                                                                         
export GEM_PATH="/home/rails/citus_reload/vendor/bundle:/usr/local/rvm/gems/${ruby_ver}:/usr/local/rvm/gems/${ruby_ver}@global"                                                  
export PATH="/home/rails/citus_reload/vendor/bundle/bin:/usr/local/rvm/gems/${ruby_ver}/bin:${PATH}"                                                                             
                                                                                                                                                                                  
# Passwords                                                                                                                                                                       
export SECRET_KEY_BASE=c1e42a80a2f883f0b3d02d99fe96b0df195c6515c1a93ea73e91564573c166ca7a1ef6093d7376d17ceee70247e9864a3def5fbba5503963283e3e3310c88238                           
export APP_DATABASE_PASSWORD=e772d5ecc3f52fb972d746cde72fd1bf                                                                                                                                                                                                                                                                                                     
# Execute the unicorn process                                                                                                                                                     
/home/rails/citus_reload/vendor/bundle/bin/unicorn \
  -c /etc/unicorn.conf -E production --debug