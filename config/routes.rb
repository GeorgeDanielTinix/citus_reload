Rails.application.routes.draw do
  
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Devise-User
  devise_for :users

  resources :inicios
  resources :nosotros
  resources :novedades
  resources :cursos
  resources :exams, only: %i[index show]
  resources :blogs
  resources :contacts, only: %i[new create]

  # Auth
  authenticate :user do
    scope '/admin' do
      resources :nosotros, only: %i[new create edit update destroy]
      resources :cursos, only: %i[new create edit update destroy]
      resources :blog, only: %i[new create edit update destroy]
      resources :novedades, only: %i[new create edit update destroy]
      resources :exams, only: %i[new create edit update destroy]
    end
  end
  get '/google03f55790f1cbd5fe.html', to: redirect('google03f55790f1cbd5fe.html')

  root to: 'inicios#index'
  
end
