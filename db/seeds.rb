user = User.create!(email: 'citus.english@hotmail.com', password: 'admin123', password_confirmation: 'admin123')
user.save

# Blog
4.times do |blog|
  Blog.create!(title: "nora ceo citus school of english #{blog}", content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu ')
end
puts ' Four Nosotros have been created con Exito ! '

# Cursos
4.times do |cursos|
  Curso.create!(title: "nora ceo citus school of english #{cursos}", content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu ')
end
puts ' Cursos have been created! '

# Examenes Internacionales
4.times do |examen|
  Examene.create!(title: "TRINITY: #{examen}", content: 'Trinity College London ha estado proporcionando evaluaciones desde 1877. Cada año más de 600.000 candidatos en más de 60 países toman una evaluación Trinity, para que los alumnos puedan medir sus logros en cada etapa de su desarrollo y en todos los niveles de competencia. Los exámenes se centran en la evaluación de las habilidades y la eficacia con que los candidatos pueden aplicar lo que han aprendido, no sólo en el conocimiento por sí mismo.')
end
puts ' Examenes have been created!'

# Nosotros
4.times do |nos|
  Nosotro.create!(title: "nora ceo citus school of english #{nos}", content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ')
end

puts ' Four Nosotros have been created con Exito ! '

# Nodedades

novedad = Novedades.create!(title: 'LAS EMOCIONES EN EL AULA', content: 'Estas son algunas de las estrategias que utilizamos en el Citus School of English para estimular la Inteligencia Emocional de nuestros alumnos: Les enseñamos a dar nombre y reconocer los sentimientos y/o emociones en Inglés. Son de gran ayuda los juegos, cuentos, historias, tarjetas con dibujo, etc. Ayudarlos a que dibujen los rostros de los personajes ilustrando la emoción que están sintiendo (Happy, Sad, Angry, Nervous, Scared, etc). Dejamos que los niños expresen sus sentimientos y emociones. La capacidad de saber qué está pasando en nuestro cuerpo y qué estamos sintiendo, son dos de los pilares fundamentales para desarrollar la Inteligencia Emocional.')
novedad.save

novedad = Novedades.create!(title: 'JUGAR ES LA MEJOR MANERA DE APRENDER', content: 'Aunque los chicos tienen una capacidad innata natural para aprender cualquier idioma extranjero, no lo aprenden bien si encuentran las clases aburridas, como suele ocurrir en muchas academias de inglés. En la actualidad, diversos enfoques y estudios en psicopedagogía coinciden en señalar que los niños aprenden mejor a través de juegos y actividades que les resulten interesantes. Las diferentes actividades basadas en juegos permiten atender aspectos importantes para el desarrollo cognitivo del niño, como: la participación en clases, impulsar la creatividad y originalidad, la creación de un comportamiento en clase')
novedad.save
