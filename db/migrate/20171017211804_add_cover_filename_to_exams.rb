class AddCoverFilenameToExams < ActiveRecord::Migration[5.1]
  def change
    add_column :exams, :cover_filename, :string
  end
end
