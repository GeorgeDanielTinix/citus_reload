class AddSlugToNovedades < ActiveRecord::Migration[5.1]
  def change
    add_column :novedades, :slug, :string
    add_index :novedades, :slug, unique: true
  end
end
