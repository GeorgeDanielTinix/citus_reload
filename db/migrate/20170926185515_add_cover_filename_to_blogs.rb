class AddCoverFilenameToBlogs < ActiveRecord::Migration[5.1]
  def change
    add_column :blogs, :cover_filename, :string
  end
end
