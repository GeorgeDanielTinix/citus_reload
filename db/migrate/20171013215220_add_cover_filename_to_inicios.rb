class AddCoverFilenameToInicios < ActiveRecord::Migration[5.1]
  def change
    add_column :inicios, :cover_filename, :string
  end
end
