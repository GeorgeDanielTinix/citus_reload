class AddCoverFilenameToNosotros < ActiveRecord::Migration[5.1]
  def change
    add_column :nosotros, :cover_filename, :string
  end
end
