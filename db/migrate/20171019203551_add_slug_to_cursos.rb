class AddSlugToCursos < ActiveRecord::Migration[5.1]
  def change
    add_column :cursos, :slug, :string
    add_index :cursos, :slug, unique: true
  end
end
