class AddSlugToInicios < ActiveRecord::Migration[5.1]
  def change
    add_column :inicios, :slug, :string
    add_index :inicios, :slug, unique: true
  end
end
