class AddSlugToNosotros < ActiveRecord::Migration[5.1]
  def change
    add_column :nosotros, :slug, :string
    add_index :nosotros, :slug, unique: true
  end
end
