class AddCoverFilenameToCursos < ActiveRecord::Migration[5.1]
  def change
    add_column :cursos, :cover_filename, :string
  end
end
