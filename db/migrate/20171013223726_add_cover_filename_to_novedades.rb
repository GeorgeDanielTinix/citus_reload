class AddCoverFilenameToNovedades < ActiveRecord::Migration[5.1]
  def change
    add_column :novedades, :cover_filename, :string
  end
end
