module ApplicationHelper
  def active_action?(params, action_controller)
    'active' if params[:controller] == action_controller
  end
end
