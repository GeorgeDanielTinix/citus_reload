class CursosController < ApplicationController
  before_action :find_curso, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @cursos = Curso.all
  end

  def show; end

  def new
    @curso = Curso.new
  end

  def edit
  end

  def create
    @curso = Curso.new(curso_params)

    if @curso.save
      redirect_to @curso, notice: 'Curso was successfully created!'
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @curso.update(curso_params)
        format.html { redirect_to @curso, notice: 'Curso was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @curso.destroy
    redirect_to curso_url, notice: 'Curso was successfully destroyed!'
  end

  private

  def find_curso
    @curso = Curso.friendly.find(params[:id])
  end

  def curso_params
    params.require(:curso).permit(:title, :content, :cover)
  end
end
