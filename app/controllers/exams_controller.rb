class ExamsController < ApplicationController
  before_action :find_examen, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @exams = Exam.all
  end

  def show; end

  def new
    @exam = Exam.new
  end

  def edit; end

  def create
    @exam = Exam.new(examen_params)

    if @exam.save
      redirect_to @exam, notice: 'Examen was successfully created!'
    else
      render :new
    end
  end

  def update
    if @exam.update(examen_params)
      redirect_to @exam, notice: 'Examen wassuccessfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @exam.destroy
    redirect_to exams_url, notice: 'Examen was successfully destroyed!'
  end

  private
  
  def find_examen
    @exam = Exam.find(params[:id])
  end

  def examen_params
    params.require(:exam).permit(:title, :content, :cover)
  end
end
