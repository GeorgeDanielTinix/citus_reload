class IniciosController < ApplicationController
  before_action :find_inicio, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @inicios = Inicio.all
  end

  def show; end

  def new
    @inicio = Inicio.new
  end

  def edit; end

  def create
    @inicio = Inicio.new(inicio_params)

    if @inicio.save
      redirect_to @inicio, notice: 'Inicio was successfully created!'
    else
      render :new
    end
  end

  def update
    if @inicio.update(inicio_params)
      redirect_to @inicio, notice: 'Inicio was successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @inicio.destroy
    redirect_to inicio_url, notice: 'Inicio was successfully destroyed!'
  end

  private

  def find_inicio
    @inicio = Inicio.friendly.find(params[:id])
  end

  def inicio_params
    params.require(:inicio).permit(:title, :content, :cover)
  end
end
