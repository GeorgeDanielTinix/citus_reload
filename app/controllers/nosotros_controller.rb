class NosotrosController < ApplicationController
  before_action :find_nosotros, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @nosotros = Nosotro.all
  end

  def show
  end

  def new
    @nosotro = Nosotro.new
  end

  def edit; end

  def create
    @nosotro = Nosotro.new(nosotro_params)

    if @nosotro.save
      redirect_to @nosotro, notice: 'Nosotros was successfully created!'
    else
      render :new
    end
  end

  def update
    if @nosotro.update(nosotro_params)
      redirect_to @nosotro, notice: 'Nosotros was successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @nosotro.destroy
    redirect_to nosotros_url, notice: 'Nosotros was successfully destroyed!'
  end

  private

  def find_nosotros
    @nosotro = Nosotro.friendly.find(params[:id])
  end

  def nosotro_params
    params.require(:nosotro).permit(:title, :content, :cover)
  end
end
