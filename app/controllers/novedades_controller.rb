class NovedadesController < ApplicationController
  before_action :find_novedad, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @novedades = Novedades.all
  end

  def show; end

  def new
    @novedades = Novedades.new
  end

  def edit; end

  def create
    @novedades = Novedades.new(novedades_params)
    if @novedades.save
      redirect_to @novedades, notice: 'Novedades was successfully created.'
    else
      render :new
    end
  end

  def update
    if @novedades.update(novedades_params)
      redirect_to @novedad, notice: 'Novedades was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @novedades.destroy
    redirect_to novedades_url, notice: 'Novedades was successfully destroyed.'
  end

  private

  def find_novedades
    @novedades = Novedades.friendly.find(params[:id])
  end

  def novedades_params
    params.require(:novedades).permit(:title, :content, :cover)
  end
end
