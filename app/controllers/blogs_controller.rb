class BlogsController < ApplicationController
  before_action :find_blog, only: %i[show edit update destroy]
  before_action :authenticate_user!, except: %i[index show]

  def index
    @blogs = Blog.all.map(&:cover_filename)
    @blogs = Blog.paginate(:page => params[:page], per_page: 5)
    # redirect_to root_path if @blogs.empty? # si no tiene blog me redirecciona root_path 
  end

  def show; end

  def new
    @blog = Blog.new
  end

  def edit; end

  def create
    @blog = Blog.new(blog_params)

    if @blog.save
      redirect_to @blog, notice: 'Blog was successfylly created!'
    else
      render :new
    end
  end

  def update
    if @blog.update(blog_params)
      redirect_to @blog, notice: 'Blog was successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to blog_url, notice: 'Blog was successfully destroyed!'
  end

  private

  def find_blog
    @blog = Blog.friendly.find(params[:id])
  end

  def blog_params
    params.require(:blog).permit(:title, :content, :cover, :autor, :fecha)
  end
end
