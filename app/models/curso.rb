class Curso < ApplicationRecord
  self.table_name = 'cursos'

  validates :title, presence: true
  validates :title, uniqueness: true
  validates :content, presence: true
  validates :cover_filename, allow_blank: true, format: {
    with: %r{\.(jpg|jpeg|png)\Z}i,
    message: 'Must be a URL for GIF, JPG or PNG image.'
  }


# Add FriendlyId to Cursos
  extend FriendlyId
  friendly_id :title, use: :slugged

  

  attr_accessor :cover

  after_save :save_cover_image, if: :cover

  def save_cover_image
    filename = cover.original_filename
    folder = "public/cursos/#{id}/cover"

    FileUtils::mkdir_p folder
    f = File.open File.join(folder, filename), 'wb'
    f.write cover.read
    f.close

    self.cover = nil
    update cover_filename: filename
  end
end
