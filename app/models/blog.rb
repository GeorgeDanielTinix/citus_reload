class Blog < ApplicationRecord
  self.table_name = 'blogs'

  validates :title, presence: true
  validates :title, uniqueness: true
  validates :content, presence: true
  validates :autor, presence: true
  validates :fecha, presence: false
  validates :cover_filename, allow_blank: true, format: { with: %r{\.(gif|jpg|png)\Z}i, message: 'Must be a URL for GIF, JPG or PNG image.'}
  

  # validates :cover_filename, presence: true
  # validate :cover_size_validations

# Add FriendlyId to blogs
  extend FriendlyId
  friendly_id :title, use: :slugged


  attr_accessor :cover


  after_save :save_cover_image, if: :cover
  #after_validation :file_field :cover, :on => :create|update, :allow_blank => true, :allow_nil => true

  def save_cover_image
    filename = cover.original_filename
    folder = "public/blogs/#{id}/cover"

    FileUtils::mkdir_p folder
    f = File.open File.join(folder, filename), 'wb'
    f.write cover.read
    f.close

    self.cover = nil
    update cover_filename: filename
  end

end
