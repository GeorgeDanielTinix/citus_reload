class Novedades < ApplicationRecord
  self.table_name = 'novedades'

  validates :title, presence: true
  validates :title, uniqueness: true
  validates :content, presence: true
  validates :cover_filename, allow_blank: true, format: {
    with: %r{\.(jpg|jpeg|png)\Z}i,
    message: 'Must be a URL for GIF, JPG or PNG image.'
  }

  # Add FriendlyId to Novedad
  extend FriendlyId
  friendly_id :title, use: :slugged

  # validate :cover_size_validations

  attr_accessor :cover

  after_save :save_cover_image, if: :cover

  def save_cover_image
    filename = cover.original_filename
    folder = "public/novedades/#{id}/cover"

    FileUtils::mkdir_p folder
    f = File.open File.join(folder, filename), 'wb'
    f.write cover.read
    f.close

    self.cover = nil
    update cover_filename: filename
  end

  # Cover validation size

  # def cover_size_validations
  #   errors[:cover_novedades] << "La imagen es muy grande deberias subir una imagen menor a 20MB" if cover_filename.size > 20.megabytes
  # end
end
