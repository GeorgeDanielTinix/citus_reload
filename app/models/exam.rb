class Exam < ApplicationRecord
  self.table_name = 'exams'
  
  validates :title, presence: true, length: { minimum: 5 }
  validates :title, uniqueness: true

  validates :cover_filename, allow_blank: true, format: { with: %r{\.(gif|jpg|png)\Z}i, message: 'Must be a URL for GIF, JPG or PNG image.'}

# Add FriendlyId to Examenes
  #extend FriendlyId
  #friendly_id :title, use: :slugged

  # validates :cover_filename, presence: true
  # validate :cover_size_validations
  attr_accessor :cover

  after_save :save_cover_image, if: :cover

  def save_cover_image
    filename = cover.original_filename
    folder = "public/examenes/#{id}/cover"

    FileUtils::mkdir_p folder
    f = File.open File.join(folder, filename), 'wb'

    f.write cover.read
    f.close

    self.cover = nil
    update cover_filename: filename
  end
end
