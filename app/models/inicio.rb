class Inicio < ApplicationRecord
  self.table_name = 'inicios'

  validates :title, presence: true
  validates :title, uniqueness: true
  validates :content, presence: true
  validates :cover_filename, allow_blank: true, format: { 
    with: %r{\.(jpeg|jpg|png)\Z}i,
    message: 'Must be a URL for GIF, JPG or PNG image.'
  }


# Add FriendlyId to Inicio
  extend FriendlyId
  friendly_id :title, use: :slugged

  attr_accessor :cover
  after_save :save_cover_image, if: :cover

  def save_cover_image
    filename = cover.original_filename
    folder = "public/inicios/#{id}/cover"

    FileUtils::mkdir_p folder
    f = File.open File.join(folder, filename), 'wb'
    f.write cover.read
    f.close

    self.cover = nil
    update cover_filename: filename
  end
end
